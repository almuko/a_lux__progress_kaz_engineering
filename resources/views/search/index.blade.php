@extends('layouts.app')
@section('content')
    <main class="main-content catalog-page search-page">
        <div class="container">
            <form method="get" action="{{ route('catalogsSearch') }}" class="search-input">
                <input type="text" name="keyword" value="{{$query or ''}}">
                <button type="submit">
                    <img src="../images/search.png" alt="">
                </button>
            </form>
            <div class="searcher">
                {{-- @php dd($projects, $catalogs) @endphp --}}
                @if($questionnaires->count() == 0 && $subcategories->count() == 0 && $products->count() == 0 && $catalogs->count() == 0)
                    По запросу {{ "".$query."" }} ничего не найдено.
                @else
                    <h2>
                        Результаты по запросу: <br> <span style="color: #ff6400; font-size: 25px;">{{ "".$query."" }}</span>
                    </h2>
                   
                @endif
            </div>
                    <div class="search-wrapper">
                        <div class="catalog-wrapper">
                            <div class="catalog-inner">
                                <div class="catalog-inner-box">
                                    @foreach($subcategories as $subcategory)
                                    @php
                                    $products = $subcategory->products;
                                    if($query!=""){
                                     $products=  \App\Product::where("category_id",$subcategory->id)->where("name","like","%".$query."%")->orderBy('sort', 'ASC')->get();
                                    }
                                    App\Helpers\TranslatesCollection::translate($products, app()->getLocale())
                                @endphp
                                 @if($products->count() > 0)
                                 <div class="catalog-category catalog-inner-box_catalog"
                                      id="block_{{ $subcategory->id }}">
                                     <div class="catalog-links">
                                     {{-- @include('/partials/product', compact('products')) --}}
                                     @foreach ($products as $product)
                                     <a href="{{ route('product', ['id' => $product->id]) }}">{{ $product->name }}</a>
                                     @endforeach
                                    </div>
                                 </div>
                             @endif
                             @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($questionnaires->count() > 0)
                    <div class="search-wrapper">
                        @endif
                        <div class="download-wrapper">
                            @foreach ($catalogs as $catalog )
                            <a href="{{ Voyager::image(json_decode($catalog->file)[0]->download_link) }}">{{ $catalog->name }}</a>
                            @endforeach
                            @foreach($questionnaires as $questionnaire)
                                <div class="download-list-wrapper">
                                    <a href="{{ Voyager::image(json_decode($questionnaire->file)[0]->download_link) }}" target="_blank">{{ $questionnaire->name }}</a>
                                </div>
                            @endforeach
                        </div>
                        </div>
                    </div>
            <style>
                .width1100 {
                    width: 100px;
                    height: 300px !important;
                }

                .prop {
                    width: 100%;
                }

                .prop_img {
                    width: 100%;
                    padding-top: 59%;
                }

                .prop_img_src {
                    position: absolute;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                    background-size: cover;
                    background-position: center center;
                }

                .prop_img_src img {
                    position: absolute;
                    left: 0;
                    top: 0;
                    width: 100%;
                    height: 100%;
                }

                .modal-image-inform::before {
                    transition: 0.6s;
                }

                .mfp-arrow {
                    margin-top: 50px;
                }
                .search-wrapper {
                    border-bottom: 0;
                }
            </style>
@endsection